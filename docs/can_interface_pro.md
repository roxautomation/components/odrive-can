# Cansimple interface Odrive Pro


See also original odrive docs:

* [can guide](https://docs.odriverobotics.com/v/latest/guides/can-guide.html)
* [cansimple protocol](https://docs.odriverobotics.com/v/latest/manual/can-protocol.html#can-protocol)


---------------------------------------
generated from dbc file by `scripts/gen_dbc_docs.py`

--8<-- "scripts/odrive-cansimple-0.6.10.md"


