## Prepare odrive

This folder contains scripts for setting up and testing odrive *via USB*


### Odrivetool

* `pip install odrive==0.6.8` -> use this version
* `dump_errors(dev0)` = show device errors.


### Rererences

* [odrive v 0.5.4](https://docs.odriverobotics.com/v/0.5.4/index.html)
